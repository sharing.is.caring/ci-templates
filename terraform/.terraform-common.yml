variables:
  SCAN_TOOL: tfsec
  DEBUG: 'false'
  TF_VARS: ''
  PLUGIN_DIR: ''
  SILENT_FAIL: 'true'
  TFSEC_CONFIG: ''
  TFSEC_CUSTOM_CHECK_URL: ''


.terradoc:
  variables:
    MODULE_PATH: "."
  stage: lint
  script:
  - git config --global user.name "${GITLAB_USER_NAME}"
  - git config --global user.email "${GITLAB_USER_EMAIL}"
  - set +ex
  - git clone ${CI_REPOSITORY_URL}
  - cd ${CI_PROJECT_NAME}
  -  git checkout ${CI_COMMIT_REF_NAME}
  - terraform-docs markdown table --output-file README.md --output-mode inject $MODULE_PATH
  - git commit -am "[ci skip] ${CI_COMMIT_REF_NAME} Gitlab CI update terraform docs $(date)."
  - git push -o ci-skip "https://${GITLAB_USER_LOGIN}:${CI_DEPLOY_TOKEN}@${CI_REPOSITORY_URL#*@}" ${CI_COMMIT_REF_NAME}
  - set -e
  rules:
  - if: $CI_COMMIT_BRANCH == "main"
    when: always

.validate:
  stage: lint
  script:
  - cd tests
  - |
    cat <<- EOF > ~/.terraformrc
    credentials "gitlab.com" {
      token = "${OLD_CI_DEPLOY_TOKEN}"
      }
    EOF
  - cat ~/.terraformrc
  - terraform init -backend=false ${PLUGIN_DIR}
  - terraform validate
  rules:
  - if: $CI_COMMIT_BRANCH != "main"
    when: always

.tfsec_scan:
  variables:
    DEBUG: 'false'
    TF_VARS: ''
    PLUGIN_DIR: ''
    SILENT_FAIL: 'true'
    TFSEC_CONFIG: ''
    TFSEC_CUSTOM_CHECK_URL: ''
  stage: scan
  before_script: 
    - if [ "$DEBUG" == "true" ]; then export TF_LOG=TRACE && CMD_OPTS="--debug"; else
        CMD_OPTS="";fi
    - if [ "$TF_VARS" == "" ]; then echo "no tfsec config file provided"; else CMD_OPTS="${CMD_OPTS}
        --tfvars-file $TF_VARS";fi
    - if [ "$TFSEC_CONFIG" == "" ]; then echo "no tfsec config file provided"; else
        CMD_OPTS="${CMD_OPTS} --config-file $TFSEC_CONFIG";fi
    - if [ "$TFSEC_CUSTOM_CHECK_URL" == "" ]; then echo "no custom check url provided";
        else CMD_OPTS="${CMD_OPTS} --custom-check-url $TFSEC_CUSTOM_CHECK_URL";fi
    - if [ "$PLUGIN_DIR" == "" ]; then echo "no plugin directory provided"; else PLUGIN_OPT="-plugin-dir=${PLUGIN_DIR}";fi
    - if [ "$SILENT_FAIL" == "true" ]; then set +e; fi
  script:
    - terraform init -backend=false $PLUGIN_OPT
    - tfsec $CMD_OPTS --format default,json --out gl-sast-report .
  artifacts:
    paths:
    - gl-sast-report.json
    reports:
      sast: gl-sast-report.json
  rules:
  - if: $SCAN_TOOL == "tfsec"
    when: always

    
.upload_module:
  stage: upload
  variables:
    TERRAFORM_MODULE_DIR: "${CI_PROJECT_DIR}"
    TERRAFORM_MODULE_NAME: module-name
    TERRAFORM_MODULE_SYSTEM: system
    TERRAFORM_MODULE_VERSION: "${CI_COMMIT_TAG}"
  script:
  - TERRAFORM_MODULE_NAME=$(echo "${TERRAFORM_MODULE_NAME}" | tr " _" -)
  - TARBALL=${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz
  - touch ${TARBALL}
  - tar -vczf ${TARBALL} -C ${TERRAFORM_MODULE_DIR} --exclude=./.git --exclude=./${TARBALL} .
  - 'curl --location --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${TARBALL}
    ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${TERRAFORM_MODULE_NAME}/${TERRAFORM_MODULE_SYSTEM}/${TERRAFORM_MODULE_VERSION}/file'
  rules:
  - if: "$CI_COMMIT_TAG"
  
.test_release:
  script:
  - |
    cat <<- EOF > ~/.terraformrc
    credentials "gitlab.com" {
      token = "${CI_DEPLOY_TOKEN}"
      }
    EOF
  - cat ~/.terraformrc
  - cd tests
  - terraform init
  - terraform apply --auto-approve -var "ssh_key=${SSH_KEY}"

.tf_init_gitlab:
  script: 
    - |
        terraform init \
            -plugin-dir=${PLUGIN_DIR} \
            -backend-config=address=${TF_ADDRESS} \
            -backend-config=lock_address=${TF_ADDRESS}/lock \
            -backend-config=unlock_address=${TF_ADDRESS}/lock \
            -backend-config=username=${TF_USERNAME} \
            -backend-config=password=${CI_DEPLOY_TOKEN} \
            -backend-config=lock_method=POST \
            -backend-config=unlock_method=DELETE \
            -backend-config=retry_wait_min=5
 

  
.tf_init_gitlab_download_plugins:
   script: 
    - |   
        terraform init \
            -backend-config=address=${TF_ADDRESS} \
            -backend-config=lock_address=${TF_ADDRESS}/lock \
            -backend-config=unlock_address=${TF_ADDRESS}/lock \
            -backend-config=username=${TF_USERNAME} \
            -backend-config=password=${CI_DEPLOY_TOKEN} \
            -backend-config=lock_method=POST \
            -backend-config=unlock_method=DELETE \
            -backend-config=retry_wait_min=5


.tf_init:
  script:
    - terraform init -plugin-dir=${PLUGIN_DIR}
  
.tf_init_download_plugins:
  terraform init 


########################################################
############### Check TF Format ########################
########################################################

.tf_check_fmt:
  script:
    - | 
        set +e
        terraform fmt -check -recursive
        result=$?
        echo "Last exist code: $result"
        echo "ran from: $(pwd)"
        set -e
        if [ $result -ne 0 ]; then
            echo "There are formatting errors. Please review the changes below."
            echo "If the changes are acceptable, run "terraform fmt -recursive" on the root project and update the merge request."
            terraform fmt -diff -recursive
            exit 1
        fi


########################################################
############### Auto TF Format #########################
########################################################


.tf_autofmt:
  script:
    - |
        git config --global user.name "${GITLAB_USER_NAME}"
        git config --global user.email "${GITLAB_USER_EMAIL}"
        git clone ${CI_REPOSITORY_URL}
        cd ${CI_PROJECT_NAME}
        git checkout ${CI_COMMIT_REF_NAME}
        terraform fmt -recursive
        git commit -am "[ci skip] ${CI_COMMIT_REF_NAME} Gitlab CI auto-format tf files."    
        git push "https://${GITLAB_USER_LOGIN}:${CI_DEPLOY_TOKEN}@${CI_REPOSITORY_URL#*@}" ${CI_COMMIT_REF_NAME}


