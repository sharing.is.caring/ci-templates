.pull_rootfs_ubuntu:
  variables:
  #tags: 
  #  - kubernetes
  #stage: pull_image
  script:
    - mkdir -p builds && cd builds
    - wget "https://partner-images.canonical.com/core/${RELEASE}/current/ubuntu-${RELEASE}-core-cloudimg-${ARCH}-root.tar.gz"
    - ls -ltra

.buildah_login:
  before_script:
    - buildah login -u gitlab-deploy-token -p $CI_DEPLOY_TOKEN $CI_REGISTRY

# Default rules policy for the jobs 
.rules_policy:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "main"'
      variables:
        DEPLOYVER: "latest"
      when: always
    - if: '$CI_COMMIT_BRANCH != "main"'
      changes:
        - .gitlab-ci.yml
        - scripts/*
        - .gitlab-ci/*
      variables:
        DEPLOYVER: $CI_COMMIT_SHA   
      when: on_success
    - if: $CI_COMMIT_BRANCH == "main"
      variables:
        DEPLOYVER: "latest"
      when: on_success

.container_scan: 
  resource_group: SCAN_SERVICE
  extends:
    - .rules_policy
  before_script:
    - echo $CI_REGISTRY_IMAGE/$IMAGENAME:${DEPLOYVER}-$ARCH
  stage: scan
  image: 
    name: anchore/inline-scan:latest
    pull_policy: ["if-not-present"]
  services:
  - name: anchore/inline-scan:latest
    alias: anchore-engine
    command: ["start"]
  script:
    - anchore-cli system wait
    - anchore-cli registry add "$CI_REGISTRY" "gitlab-deploy-token" "$CI_DEPLOY_TOKEN" --skip-validate 
    - anchore_ci_tools.py -a -r --timeout 1000 --image $CI_REGISTRY_IMAGE/$IMAGENAME:${DEPLOYVER}-$ARCH
  artifacts:
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}
    paths:
    - anchore-reports/*


.buildah_ubi:
  variables:
    RELEASE: ubi8
    FLAVOR: ubi-minimal
  before_script:
    - buildah login -u ${RHEL_REGISTRY_USER} -p ${RHEL_REGISTRY_TOKEN} registry.redhat.io
    - baseimage=$(buildah --arch $ARCH from registry.redhat.io/${RELEASE}/${FLAVOR})
    - echo $baseimage
    - mntbase=$(buildah mount $baseimage)


.buildah_scratch_fedora:
  variables:
    PACKAGE_LIST: networkmanager firewalld sudo vim less iputils microdnf
    RELEASE: 35
  script:
    - baseimage=$(buildah --arch $ARCH from scratch)
    - echo $baseimage
    - mntbase=$(buildah mount $baseimage)
    - dnf install --installroot=$mntbase --releasever=$RELEASE --assumeyes $PACKAGE_LIST

.buildah_scratch_ubuntu:
  script:
    - baseimage=$(buildah --arch $ARCH from scratch)
    - echo $baseimage
    - mntbase=$(buildah mount $baseimage)
    - buildah add $baseimage "./builds/ubuntu-${RELEASE}-core-cloudimg-${ARCH}-root.tar.gz" "/"

.buildah_scratch_arch:
  variables:
    PACKAGE_LIST: busybox
  script:
    - baseimage=$(buildah --arch $ARCH from scratch)
    - echo $baseimage
    - mntbase=$(buildah mount $baseimage)
    - echo y|sudo pacstrap -i -c $mntbase $PACKAGE_LIST
    - buildah run $baseimage -- busybox ln -s /usr/bin /bin 
    - buildah run $baseimage -- busybox ln -s /usr/sbin /sbin 
    - buildah run $baseimage -- busybox --install -s



.buildah_config_push:
  script:
    - buildah config --author "${AUTHOR}" $baseimage #${AUTHOR}
    - buildah config --arch $ARCH $baseimage
    - buildah config --label Name=$IMAGENAME ${LABELS} $baseimage 
    - buildah commit $baseimage $IMAGENAME:${DEPLOYVER}-$ARCH
    - buildah images|grep $IMAGENAME|awk '{print $3}' > imagehash.txt
    - buildah unmount $baseimage
    - buildah rm $baseimage
    - export IMAGEHASH=$(cat imagehash.txt)
    - buildah login -u gitlab-deploy-token -p $CI_DEPLOY_TOKEN $CI_REGISTRY
    - buildah push $IMAGEHASH $CI_REGISTRY_IMAGE/$IMAGENAME:${DEPLOYVER}-$ARCH
    - buildah push $IMAGEHASH $CI_REGISTRY_IMAGE/$IMAGENAME/$ARCH:${DEPLOYVER}
    - buildah rmi $IMAGEHASH

.buildah_template_ubuntu:
  extends:
    - .pull_rootfs_ubuntu
    - .buildah_scratch_ubuntu
    - .buildah_config_push
    - .rules_policy
  script:
      - echo "When extending this job template, Add a script block with your container customizations"
      - echo "It should override this script block, which is just an example"
      - buildah run $baseimage -- apt-get update 2&>1
      - buildah run apt-get install -qq git >/dev/null 2>&1

.buildah_template_arch:
  variables:
    PACKAGE_LIST: mntcont busybox util-linux libarchive wget binutils gawk wget which sed patch pacman gzip 
  extends:
    - .buildah_scratch_arch
    - .rules_policy
  script:
      - echo "When extending this job template, Add a script block with your container customizations"
      - echo "It should override this script block, which is just an example"
      - !reference [.buildah_config_push, script]

.buildah_template_ubi:
  extends:
    - .buildah_ubi
    - .rules_policy
  script:
      - echo "When extending this job template, Add a script block with your container customizations"
      - echo "It should override this script block, which is just an example"
      - buildah run $baseimage -- microdnf install curl unzip git 2&>1
      - !reference [.buildah_config_push, script]

.buildah_template_fedora:
  extends:
    - .buildah_scratch_fedora
    - .rules_policy
  script:
      - echo "When extending this job template, Add a script block with your container customizations"
      - echo "It should override this script block, which is just an example"
      - buildah run $baseimage -- dnf update -y -q
      - buildah run $baseimage -- dnf install -y $PACKAGE_LIST 2&>1
      - buildah run $baseimage -- dnf clean all
      - buildah run $baseimage -- rm -rf /var/cache/yum
      - !reference [.buildah_config_push, script]


.deploy_manifest:
  script:
    - listName=${IMAGENAME}list:${DEPLOYVER}
    - buildah manifest create $listName 
    - |
        buildah manifest add --override-arch=${ARCH}  --override-os=linux \
          --os=linux --arch=${ARCH} --variant v8 $listName docker://$CI_REGISTRY_IMAGE/${IMAGENAME}:${DEPLOYVER}-${ARCH}
    - |
        buildah manifest add --override-arch=${ARCH} --override-os=linux \
          --os=linux --arch=amd64 $listName docker://$CI_REGISTRY_IMAGE/${IMAGENAME}:${DEPLOYVER}-${ARCH} 
    - buildah manifest push --format=v2s2 $listName docker://$CI_REGISTRY_IMAGE/${IMAGENAME}:${DEPLOYVER}
    - buildah rmi $listName 

